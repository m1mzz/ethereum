import { createRouter, createWebHistory }  from 'vue-router'
import PageHome from './pages/PageHome.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: PageHome,
  },
  {
    path: '/address/:address',
    name: 'transactions',
    component: () => import('./pages/PageTransactions.vue'),
  },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
