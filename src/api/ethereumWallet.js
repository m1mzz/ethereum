import { toRealPrice, fromHex } from '@/helpers/numberTransforms';

let ethereum;

// helpers
const defineEthereum = () => {
  if (ethereum) return true
  if ('ethereum' in window) {
    ethereum = window.ethereum;
    return true;
  }
  return false;
}

const ethereumRequestErrorHandler = (err) => {
  if (err?.code === 4001) {
    // EIP-1193 userRejectedRequest error
    // If this happens, the user rejected the connection request.
    console.log('Please connect to MetaMask.');
  } else {
    console.error(err);
  }
}


// returned methods
export const getAddress = async () => {
  if (defineEthereum()) {
    const address = await ethereum.request({ method: 'eth_coinbase' }).catch(ethereumRequestErrorHandler);
    return address;
  }
  return null
}

export const getBalanceEth = async (address) => {
  const options = { method: 'eth_getBalance', params: [address, 'latest']};
  const balance = await ethereum.request(options).catch(ethereumRequestErrorHandler);
  return toRealPrice(fromHex(balance));
};

export const requestToConnect = async () => {
  if (defineEthereum()) {
    const [address] = await ethereum.request({ method: 'eth_requestAccounts' }).catch(ethereumRequestErrorHandler) || [];
    return address;
  }
  return null;
};

// MetaMask networks
export const networksCodes = {
  MAIN: '0x1',
  ROPSTEN_TEST : '0x3',
  RINKEBY_TEST : '0x4',
};
export const getWalletNetwork = async () => {
  return await ethereum.request({ method: 'eth_chainId' }).catch(ethereumRequestErrorHandler);
};

export const changeWalletNetwork = async (networkId) => {
  return await ethereum.request({
    method: 'wallet_switchEthereumChain',
    params: [{ chainId: networkId }],
  }).catch(ethereumRequestErrorHandler);
};

export const networkChangeListener = (callback) => {
  if (defineEthereum()) {
    ethereum.on('chainChanged', (chainId) => {
      if (callback) {
        callback(chainId);
      }
    });
    return true;
  }
  return false;
};

// init
defineEthereum();
