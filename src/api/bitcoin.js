export const getBTC = async () => {
  const res = await fetch('https://blockchain.info/ticker').catch(console.error)
  if (res?.status === 200) {
    const data = await res.json();
    return data;
  }
  return null;
}
