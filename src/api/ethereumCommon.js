import { toRealPrice } from '@/helpers/numberTransforms';
const { VITE_API_ETH_KEY } = import.meta.env;

const apiUrl = 'https://api-rinkeby.etherscan.io/api';
const getUrl = (optionsObj) => {
  const optionsWithKey = { ...optionsObj, key: VITE_API_ETH_KEY };
  return apiUrl + '?' + new URLSearchParams(optionsWithKey)
};
const STATUS_OK = '1';

export const getBalanceOnx = async (address) => {
  const options = {
    module: 'account',
    action: 'tokenbalance',
    contractaddress: '0xa0dff2d4462ec3b60a7ab6dfd5071a98751151d9',
    tag: 'latest',
    address,
  }
  const res = await fetch(getUrl(options)).catch(console.error)
  if (res?.status === 200) {
    const data = await res.json();
    if (data?.status === STATUS_OK) {
      return toRealPrice(data.result);
    }
  }
  return 0;
}

export const  transactionMethodTypes = {
  TRANSFER: "0x",
  LIST_NFT: "0x45432dbb",
  SETTLE_LIST: "0x8815f110",
}

export const getAllTransactions = async (address, options) => {
  const defaultOptions = {
    module: 'account',
    action: 'txlist',
    startblock: 0,
    endblock: 99999999,
    sort: 'asc',
    address,
  }
  const res = await fetch(getUrl({ ...defaultOptions, ...options })).catch(console.error)
  if (res?.status === 200) {
    const data = await res.json();
    if (data?.status === STATUS_OK) {
      return data.result;
    }
  }
  return [];
}
