export const fromHex = (hexString) => {
  const result = Number(hexString);
  if (!result || isNaN(result)) {
    console.error('Invalid hex string: ' + hexString);
    return 0;
  }
  return result;
}

export const toRealPrice = (number) => {
  if (!number) return 0;
  const strOfNumber = number.toString();
  return parseFloat(strOfNumber.slice(0, -18) + '.' + strOfNumber.slice(-18));
}
