import { reactive, ref, computed } from 'vue';
import { getBalanceOnx } from '@/api/ethereumCommon';
import { 
  getBalanceEth,
  getAddress,
  requestToConnect,
  networksCodes,
  getWalletNetwork,
  changeWalletNetwork,
  networkChangeListener,
} from '@/api/ethereumWallet';

// returned data
const isConnected = ref(false);
const wallet = reactive({
  address: '',
  eth: 0,
  onx: 0,
  networkId: '',
})

const errorMessage = ref('');
const setErrorMessage = (str = '') => {
  errorMessage.value = str;
}

const isWrongNetwork = computed(() => wallet.networkId !== networksCodes.RINKEBY_TEST);
const switchToRightNetwork = () => changeWalletNetwork(networksCodes.RINKEBY_TEST);
const setNetwork = (networkId) => {
  if (networkId) {
    wallet.networkId = networkId;
  }
}
const defineNetwork = async () => {
  const networkId = await getWalletNetwork();
  setNetwork(networkId);
}
// add network listener 
networkChangeListener(setNetwork);


const initBalance = async (address) => {
  const [eth = 0, onx = 0] = await Promise.all([
    getBalanceEth(address),
    getBalanceOnx(address),
  ])
  wallet.eth = eth;
  wallet.onx = onx;
}

const initWallet = async () => {
  const address = await getAddress();
  if (address) {
    wallet.address = address;
    initBalance(address);
    await defineNetwork();
    isConnected.value = true;
  }
}

const connect = async () => {
  setErrorMessage();
  const address = await requestToConnect();
  if (address) {
    wallet.address = address;
    initBalance(address);
    await defineNetwork();
    isConnected.value = true;
    return true;
  }
  setErrorMessage('Can\'t connect to your wallet');
}


export default function UseEthereum () {
  if (!isConnected.value) {
    initWallet()
  }
  return {
    isConnected,
    errorMessage,
    wallet,
    connect,
    isWrongNetwork,
    switchToRightNetwork,
  }
}
